from machine import Pin
from time import sleep, ticks_ms, ticks_diff
import time
import ntptime

from led import Led, Gradient, COLOR_OFF, COLOR_GREEN, COLOR_RED
from wifi import connect_wifi
from umqttsimple import MQTTClient

from settings import WIFI_SSID, WIFI_PASSWORD
from settings import MQTT_HOST, MQTT_PORT, MQTT_USERNAME, MQTT_PASSWORD
from settings import MEDICATION_NAME

LONG_PRESS_MS = 1000

p_button = Pin(14, Pin.IN, Pin.PULL_UP)
led = Led(22)
mqtt = MQTTClient(MEDICATION_NAME, MQTT_HOST, port=MQTT_PORT, user=MQTT_USERNAME, password=MQTT_PASSWORD)


def main():
    signal_startup()
    run_loop()


def signal_startup():
    for color in [(255, 0, 0), (0, 255, 0), (0, 0, 255)]:
        led.set_color(*color)
        sleep(0.2)
        led.set_color(0, 0, 0)

def run_loop():
    while True:
        detect_press()
        led.update()
        sleep(0.05)


def detect_press():
    if p_button.value() == 0:
        press_type = get_press_type(p_button, 0)
        
        if press_type == "SHORT":
            send_applied_drops_time()
        elif press_type == "LONG":
            send_opened_drops_time()


def get_press_type(pin, pressed_state):
    start_ms = ticks_ms()
    while True:
        sleep(0.1)
        if pin.value() != pressed_state:
            break

    end_ms = ticks_ms()
    
    sleep(0.1)  # Debounce
    
    if end_ms - start_ms > LONG_PRESS_MS:
        return "LONG"
    
    return "SHORT"


def send_applied_drops_time():
    print("Send applied drops")
    led.set_color(2,2,2)
    
    try:
        connect_wifi(WIFI_SSID, WIFI_PASSWORD)
        ntptime.settime()
        
        time_str = get_utc_time()
        
        print("drops applied at", time_str)
        mqtt_send(f"{MEDICATION_NAME}/use", time_str)
    
        show_success()

    except Exception as exc:
        show_failure()
        print(exc)


def send_opened_drops_time():
    print("Send applied drops")
    led.set_color(2,2,2)
    
    try:
        connect_wifi(WIFI_SSID, WIFI_PASSWORD)
        ntptime.settime()
        
        time_str = get_utc_time()
        
        print("drops applied at", time_str)
        mqtt_send(f"{MEDICATION_NAME}/open", time_str)
    
        show_success()

    except Exception as exc:
        show_failure()
        print(exc)
    
def get_utc_time():
    time_tup = time.gmtime()
    time_str = (f"{time_tup[0]:02d}-{time_tup[1]:02d}-{time_tup[2]:02d} " +
                f"{time_tup[3]:02d}:{time_tup[4]:02d}")
    
    return time_str


def show_success():
    print("operation successful")
    led.temporary_gradient(Gradient(COLOR_OFF, COLOR_GREEN, 300), 3)
    

def show_failure():
    print("operation failed")
    led.temporary_gradient(Gradient(COLOR_OFF, COLOR_RED, 300), 3)


def mqtt_send(topic, msg):
    mqtt.connect()
    mqtt.publish(topic, msg)
    mqtt.disconnect()


if __name__ == "__main__":
    main()
    