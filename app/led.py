from machine import Pin
import neopixel
from time import sleep, ticks_ms, ticks_diff
from math import sin, pi


COLOR_BLUE = (0, 0, 255)
COLOR_RED = (255, 0, 0)
COLOR_GREEN = (0, 255, 0)
COLOR_ORANGE = (0, 0, 0)
COLOR_OFF = (0, 0, 0)


def sine_y(t, amplitude):
    return (1 + sin(t * (2 * pi / amplitude) - (pi/2))) / 2


class Gradient:
    def __init__(self, rgb0, rgb1, transition_time = 2000):
        self.rgb0 = rgb0
        self.rgb1 = rgb1
        self.transition_time = transition_time
        self.ticks = ticks_ms()
    
    def update(self, setter):
        cur_ms = ticks_ms()
        ms_passed = ticks_diff(cur_ms, self.ticks)
        
        if ms_passed < 10:
            # Prevent running too fast
            return
        
        multiplier = sine_y(ms_passed, self.transition_time)
    
        rgb_floats = ( x0 + (x1 - x0) * multiplier for x0, x1 in zip(self.rgb0, self.rgb1))
        rgb = tuple( int(x) for x in rgb_floats )
    
        setter(*rgb)


class Led:
    def __init__(self, pin_number):
        self._led = neopixel.NeoPixel(Pin(pin_number), 1)
        self._gradient = None
    
    def set_color(self, r, g, b):
        self._led[0] = (r, g, b)
        self._led.write()

    def start_gradient(self, gradient):
        self._gradient = gradient
        self.update()
    
    def stop_gradient(self):
        self._gradient = None
        self.set_color(*COLOR_OFF)
    
    def temporary_gradient(self, gradient, number_cycles):
        self.start_gradient(gradient)
        beep_time = gradient.transition_time * number_cycles
        
        start = ticks_ms()
        while True:
            self.update()
            sleep(0.05)
            
            if ticks_diff(ticks_ms(), start) >= beep_time:
                self.stop_gradient()
                break

        
    
    def update(self):
        if self._gradient is not None:
            self._gradient.update(self.set_color)


if __name__ == "__main__":
    l = Led(22)
    
    l.start_gradient(Gradient(
        (0, 0, 0),
        (0, 0, 255)
    ))
    
    while True:
        l.update()
        sleep(0.05)
