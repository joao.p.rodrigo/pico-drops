# PicoDrops

Overengineered solution to record when eyedrops are applied and when a new bottle is opened to an MQTT topic.

Short press sends the timestamp of applied drops.

Long press sends the timestamp of new opened bottle.

Includes some sine wave led animations.

## Acknowledgments

* [umqttsimple - MQTT library](https://github.com/RuiSantosdotme/ESP-MicroPython/blob/master/code/MQTT/umqttsimple.py)

## Blog Post

I've written a blog post about this project in my blog, [!not_a_doctor](https://notadoctor.me/posts/pico-drops/).

## BOM

* Raspberry pico W
* WS2812b in dot PCB
* Push button
* Some wire to connect it all together

## Wiring

Wire the push button to GND and GPIO14.

Wire the LED to VSYS, GND and GPIO22.

## Software

Copy the .py files to the raspberry pico.

Include a settings.py with the following code:

```python
WIFI_SSID = "YOUR WIFI SSID"
WIFI_PASSWORD = "YOUR WIFI PASSWORD"

MQTT_USERNAME = "YOUR MQTT USERNAME"
MQTT_PASSWORD = "YOUR MQTT USER PASSWORD"
MQTT_HOST = "YOUR MQTT HOST OR IP"
MQTT_PORT = 1883
MEDICATION_NAME = "SOME_DRUG"
```
