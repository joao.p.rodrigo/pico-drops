
PICO_HOLE_DIAMETER = 2.1;
PICO_HOLE_L_SPACING = 11.4;
PICO_HOLE_D_SPACING = 47;
PICO_HOLE_STANDOFF_DIAMETER = 3.8;
PICO_USB_WIDTH = 8;
PICO_USB_HEIGHT = 3;

WALL_THICKNESS = 1.7 ;  // 0.4 * 5;

PICO_D = 51;
PICO_L = 21;

PICO_PCB_THICKNESS = 1;

L_TOLERANCE = 0.2;
D_TOLERANCE = 0.1;
H_TOLERANCE = 0.1;

STANDOFF_HEIGHT = 3;


CASE_D = PICO_D + 2*WALL_THICKNESS + 2*D_TOLERANCE;
CASE_L = PICO_D + 2*WALL_THICKNESS + 2*L_TOLERANCE;
CASE_H = 
    4 +  // extra height to clear pico components
    WALL_THICKNESS +  // bottom
    STANDOFF_HEIGHT + // ensure pico standoff clearance
    1 + // PICO PCB
    H_TOLERANCE;

CASE_RAD = 2;

// Space for the electronics

HOLE_D = PICO_D + 2*D_TOLERANCE;
HOLE_L = CASE_L - 2*L_TOLERANCE - 2*WALL_THICKNESS;

BUTTON_STAND_D = 6;
BUTTON_STAND_L = 6;
BUTTON_HEIGHT = 5;
BUTTON_PRONG_HEIGHT = 3;

BUTTON_CUTOUT_SEP = 0.2;
BUTTON_CUTOUT_D = 5;
BUTTON_CUTOUT_L = 10;

LED_HEIGHT = 4.5;  // LED + Solder joint and space for gluing
LED_PCB_DIAMETER = 9.5;
LED_COVER_H = 0.2;

CASE_HOLE_DIAMETER = PICO_HOLE_DIAMETER;
CASE_STANDOFF_DIAMETER = WALL_THICKNESS * 2;
CASE_STANDOFF_D_SPACING = CASE_D - WALL_THICKNESS * 2 - CASE_HOLE_DIAMETER/2;
CASE_STANDOFF_L_SPACING = CASE_L - WALL_THICKNESS * 2 - CASE_HOLE_DIAMETER/2;

module rounded_rectangle(d, l, h, radius){
    hull(){
        for(i=[
            [radius, radius],
            [d - radius, radius],
            [radius, l - radius],
            [d - radius, l - radius]
        ]){
            translate(i)
            cylinder(r=radius, h=h, center=false, $fn=16);
        }
    }
}

module case() {
    difference() {
        union(){
            rounded_rectangle(CASE_D, CASE_L, CASE_H, CASE_RAD);
        }

        // Rectangle Hole
        translate([WALL_THICKNESS + D_TOLERANCE, WALL_THICKNESS + L_TOLERANCE, WALL_THICKNESS]) {
            rounded_rectangle(HOLE_D, HOLE_L, CASE_H, CASE_RAD);
        }

        // USB Hole
        translate([
            0, 
            (CASE_L - PICO_USB_WIDTH) / 2 - L_TOLERANCE, 
            WALL_THICKNESS + STANDOFF_HEIGHT + PICO_PCB_THICKNESS - H_TOLERANCE
        ])
            cube([20, PICO_USB_WIDTH + 2* L_TOLERANCE, PICO_USB_HEIGHT + 2*H_TOLERANCE]);
    }
}

module pico_standoffs(){
    translate([
        (CASE_D - PICO_HOLE_D_SPACING) / 2,
        (CASE_L - PICO_HOLE_L_SPACING) / 2,
        WALL_THICKNESS
    ])
        for (i=[
            [0,0], [PICO_HOLE_D_SPACING, 0],
            [0, PICO_HOLE_L_SPACING], [PICO_HOLE_D_SPACING, PICO_HOLE_L_SPACING]
        ]) {
            translate(i)
                difference() {
                    cylinder(r=PICO_HOLE_STANDOFF_DIAMETER/2, h=STANDOFF_HEIGHT, center=false, $fn=8);
                    cylinder(r=PICO_HOLE_DIAMETER/2, h=STANDOFF_HEIGHT, center=false, $fn=5);
                }
        }
}

module button_stand(){
    button_standoff_height = CASE_H - WALL_THICKNESS - BUTTON_HEIGHT;
    
    assert(button_standoff_height > BUTTON_PRONG_HEIGHT, "button standoff prongs don't have enough clearence");

    translate([CASE_D/2, CASE_L/5, WALL_THICKNESS])
        translate([-BUTTON_STAND_D/2, -BUTTON_STAND_L/2])
            cube(size=[BUTTON_STAND_D, BUTTON_STAND_L, button_standoff_height], center=false);
}

module led_stand(){
    led_standoff_height = CASE_H - WALL_THICKNESS - LED_HEIGHT;

    translate([CASE_D/2, CASE_L*4/5, WALL_THICKNESS])
        cylinder(r=LED_PCB_DIAMETER/2, h=led_standoff_height, center=false, $fn=10);
}

module case_standoffs(){
    r = CASE_STANDOFF_DIAMETER / 2;
    
    translate([(r + CASE_D - CASE_STANDOFF_D_SPACING) / 2, (r + CASE_L - CASE_STANDOFF_L_SPACING) / 2])
        for(i=[
            [0, 0],
            [CASE_STANDOFF_D_SPACING - r, 0],
            [0, CASE_STANDOFF_L_SPACING - r],
            [CASE_STANDOFF_D_SPACING - r, CASE_STANDOFF_L_SPACING - r]
        ]) {
            translate(i)
                difference() {
                    cylinder(r=r, h=10, center=false, $fn=10);
                    cylinder(r=CASE_HOLE_DIAMETER/2, h=CASE_H, center=false);
                }
        }
}


module top_cover(){
    depression_sphere_radius = 4.2;
    depression_h = WALL_THICKNESS / 2;
    difference(){
        rounded_rectangle(CASE_D, CASE_L, WALL_THICKNESS, CASE_RAD);

        // Button depression
        translate([CASE_D/2, CASE_L/5, depression_sphere_radius + WALL_THICKNESS - depression_h]) {
            sphere(r=depression_sphere_radius, $fn=20);
        }

        // Button cutout
        translate([CASE_D/2, CASE_L/5])
            union(){
                // 180º arc separation
                difference(){
                    cylinder(r=BUTTON_CUTOUT_D / 2 + BUTTON_CUTOUT_SEP, h=100, center=false, $fn=20);
                    cylinder(r=BUTTON_CUTOUT_D / 2, h=100, center=false, $fn=20);
                    translate([- BUTTON_CUTOUT_D, 0, 0]) {
                        cube(size=[100, BUTTON_CUTOUT_D, 100], center=false);
                    }

                }

                translate([BUTTON_CUTOUT_D/2,0,0])
                    cube([BUTTON_CUTOUT_SEP, BUTTON_CUTOUT_L - BUTTON_CUTOUT_D/2, 100]);

                translate([-BUTTON_CUTOUT_D/2 - BUTTON_CUTOUT_SEP, 0, 0]) {
                    cube([BUTTON_CUTOUT_SEP, BUTTON_CUTOUT_L - BUTTON_CUTOUT_D/2, 100]);
            }
            
        }

        // light "door"
        translate([CASE_D/2, CASE_L*4/5, 0])
            cylinder(r=LED_PCB_DIAMETER/2, h=WALL_THICKNESS - LED_COVER_H, center=false);

        
    }
}

module top_insert(){
    insert_d = HOLE_D ;//- 2* D_TOLERANCE;
    insert_l = HOLE_L ;// - 2* L_TOLERANCE;
    insert_h = WALL_THICKNESS;

    usb_cutout_l = PICO_USB_WIDTH + 2* L_TOLERANCE;

    translate([(CASE_D - insert_d) / 2, (CASE_L - insert_l) / 2])
        difference() {
            rounded_rectangle(insert_d, insert_l, insert_h, CASE_RAD);
            translate([WALL_THICKNESS, WALL_THICKNESS])
                rounded_rectangle(insert_d - 2*WALL_THICKNESS, insert_l - 2*WALL_THICKNESS, insert_h, CASE_RAD);

            // USB cutout
            translate([0, (insert_d - usb_cutout_l)/2, 0])
                #cube([20, usb_cutout_l, insert_h]);
        }
}


module bottom(){
    case();
    pico_standoffs();
    button_stand();
    led_stand();
}

module top(){
    translate([-10, 0, WALL_THICKNESS *2]) {
        rotate([0, 180, 0]) {
            translate([0, 0, WALL_THICKNESS])
                top_cover();
            top_insert();
        }
    }
}

bottom();

top();
    